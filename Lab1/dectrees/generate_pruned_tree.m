function [ T1, T1p, n, m ] = genPrunedTree( data, frac )
    [n,m]=size(data);
    p=randperm(n);
    data_new=data(p(1:floor(n*frac)),:);
    data_prune=data(p(floor(n*frac)+1:n),:);
    T1=build_tree(data_new);
    T1p=prune_tree(T1,data_prune);
end

