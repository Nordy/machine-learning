function majorityClass = findMajorityClass(data, vectorTuple)
%{
numberOfPositives = 0; 
numberOfNegatives = 0; 

dataSum = size(data, 1);

for k=1:dataSum
    if data(k,column1) == value1
        if data(k,column2) == value2
           if data(k,end) == 0
               numberOfNegatives = numberOfNegatives + 1; 
           elseif data(k,end) == 1
               numberOfPositives = numberOfPositives + 1; 
           end
        end
    end
end

disp('Positives:'); disp(numberOfPositives); 
disp('Negatives:'); disp(numberOfNegatives); 


if numberOfPositives < numberOfNegatives
    majorityClass = 0;
else
    majorityClass = 1; 
end
%}

for i=1:size(vectorTuple,1)
    data = subset(data,vectorTuple(i,1),vectorTuple(i,2));
end
majorityClass = majority_class(data);


end
