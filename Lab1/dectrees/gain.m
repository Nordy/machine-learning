% Machine learning lab 1 assignment 1
% Calculation of entropy for input data

function resultGain = gain(data)

numberOfAttributes = size(data,2) - 1;  % Size of attributes minus the class column
numberOfRows = size(data,1);            % Number of data points
entS = ent(data);                       % entropy of data before splitting

resultGain(numberOfAttributes) = 0;     % vector where results are stored

for i=1:numberOfAttributes,             % For each attribute 
    columnValues = data(:,[i,end]);     % Get values of current attribute and class
    uniqueAttributeValues = values(columnValues,1); %Get Set of attribute values
    sumWeightedEntropy = 0;             % Variable where entropy for each attribute is saved
    
    %disp('Number of unique attribute values.');
    %disp(size(uniqueAttributeValues,1))
    
    for k = 1:size(uniqueAttributeValues,1) %for each unique attribute value
       subsetOfColumn = subset(columnValues,1,uniqueAttributeValues(k:k));
       %disp(subsetOfColumn);
       
       entSk = ent(subsetOfColumn);     %Entropy of subset
       entWeight = size(subsetOfColumn,1)/numberOfRows ;    % weight of subset
       sumWeightedEntropy = sumWeightedEntropy + entWeight*entSk;   % Add weighted entropy
    end
    
    resultGain(i) = entS - sumWeightedEntropy;      %Gain for splitting at attribute i 
end
