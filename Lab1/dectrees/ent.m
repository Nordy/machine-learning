% Machine learning lab 1 assignment 1
% Calculation of entropy for input data

function entropy = ent(data)

% Vector of the classes for each data points
dataClassifications = data(:,end); 

%Number of elements as a scalar value
dataSum = size(dataClassifications, 1); 

%disp(dataSum);
sumOfNegativeClassifications = 0; 
sumOfPositiveClassifications = 0; 

for j=1:dataSum,
    if dataClassifications(j) == 0
        sumOfNegativeClassifications = sumOfNegativeClassifications + 1; 
    elseif dataClassifications(j) == 1
        sumOfPositiveClassifications = sumOfPositiveClassifications + 1; 
    end
end

% print the number of true and false data points
%disp('Number of Elements: '); disp(dataSum);
%disp('True: '); disp(sumOfPositiveClassifications);
%disp('False: '); disp(sumOfNegativeClassifications);

p0 = sumOfNegativeClassifications/dataSum; 
p1 = sumOfPositiveClassifications/dataSum; 

if p0 == 0
    entropy = -p1*log2(p1);
elseif p1 == 0
    entropy = -p0*log2(p0);
else
    entropy = (-p0*log2(p0))-(p1*log2(p1));
end

    
