function gainVector = gainAtCertainDepth(data, splitVector)
    if size(splitVector, 2) == 0
       gainVector = gain(data);
       return;
    end

    unsplitData = data;
    unsplitData(:, splitVector) = [];
    
    
    if size(splitVector, 2) == 1
        index = 1;
        uniqueAttributeValues = values(data, splitVector(1,index));
        
        for k = 1:size(uniqueAttributeValues,1) %for each unique attribute value
            ubset = subset(data,splitVector(1,index),uniqueAttributeValues(k:k));
            %disp(ubset);
            gainVector(k,:) = gain(ubset); 
        end
        return;
    end
    
    %disp(data);
    disp('Unsupported');
    gainVector = [];
end