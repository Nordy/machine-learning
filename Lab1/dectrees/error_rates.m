function [fracs, errors] = error_rates(train_data, test_data)
    
    i = 1;
    repeats = 10000; 
    %for frac = 0.05:0.05:1.0
    for frac = 0.3:0.1:0.8
        e_sum = 0; 
        fracs(i) = frac;
        for j = 1:repeats
            [original_tree, pruned_tree] = generate_pruned_tree(train_data, fracs(i));
            e_sum = e_sum + calculate_error(pruned_tree, test_data);
        end
        errors(i) = e_sum/repeats; 
        i = i+1;
    end
end